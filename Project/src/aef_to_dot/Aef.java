package aef_to_dot;

import java.util.ArrayList;

public class Aef {

	String group = "NOM1 NOM2 NOM3 NOM4 NOM5 NOM6";
	String name;
	ArrayList<Transition> transitions;
	ArrayList<String> acceptings;
	ArrayList<String> initials;

	Aef(){
		initials = new ArrayList<String>();
		acceptings = new ArrayList<String>();
		transitions = new ArrayList<Transition>();
	}

	public void name(String name) {
		this.name = name;
	}

	public void add_initial(String istate) {
		initials.add(istate);
	}

	public String toDot() {
		String out = new String();
		out += "\ndigraph aef{//" + group;
		out += Dot.node_decl(name, "shape=plaintext, fontname=comic, fontsize=18, fontcolor=blue");
		for (String istate : initials) {
			out += new Transition(name,"",istate).toDot();
		}
		out += "\n}\n";
		return out;
	}

	public void toDotFile(String path_file){

	}
}
