package aef_to_dot;

public class Dot {

	public static String protect(String string) {
		return "\"" + string + "\"" ;
	}
	
	public static String node(String node) {
		return protect(node);
	}
	
	public static String label(String label) {
		return " label=" + protect(label);
	}
	
	public static String options(String options) {
		return " [" + options + " ];" ;
	}
	
	public static String node_decl(String node, String options) {
		return "\n" + node(node) + options( label(node) + ", " + options );
	}
	
	public static String edge(String source, String label, String target) {
		return "\n" + protect("TODO Dot.edge");
	}
}
