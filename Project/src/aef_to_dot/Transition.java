package aef_to_dot;

public class Transition {

	String source;
	String symbol;
	String target;
	
	Transition(String source, String symbol, String target){
		this.source = source;
		this.symbol = symbol;
		this.target = target;
	}
	
	public String toDot() {
		return Dot.edge(source, symbol, target);
	}
}
